import json
import pika, AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS

        def process_approval(ch, method, properties, body):
            dic = json.loads(body)
            email = dic["presenter_email"]
            name = dic["presenter_name"]
            title = dic["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )
            print("  Request %r" % body)

        def process_rejection(ch, method, properties, body):
            dic = json.loads(body)
            email = dic["presenter_email"]
            name = dic["presenter_name"]
            title = dic["title"]
            send_mail(
                "Your presentation has been Rejected",
                f"{name}, we're happy to tell you that your presentation {title} has been Rejected",
                "admin@conference.go",
                [email],
                fail_silently=False,
            )
            print("  Request %r" % body)

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_rejections")
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
